from django import *
from django.forms import ModelForm, Textarea, TextInput
from lab_2.models import Note


#Create FriendForm
class NoteForm(ModelForm):
    class Meta :
        model = Note
        fields = "__all__" #Get all attribute from Fried attribute
        widgets = {
            'To' : forms.TextInput(attrs={'id':'id-to' ,'placeholder':'Type you name'}),
            'From' : forms.TextInput(attrs={'id':'id-from', 'placeholder' : 'Type message recipient'}),
            'Title' : forms.TextInput(attrs={'id':'id-title', 'placeholder':'Enter your title'}),
            'Message' : forms.Textarea(attrs={'id':'id-message', 'placeholder':'Enter your message'}),
        }