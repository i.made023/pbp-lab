from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab4_index.html', response)
    

@login_required(login_url = '/admin/login/')
def add_note(request):
    maps = {}

    form = NoteForm(request.POST or None, request.FILES or None)

    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-4/')
    else :
        form = NoteForm
        maps['form'] = form
        return render(request, 'lab4_form.html', maps)

@login_required(login_url = '/admin/login/')
def note_list(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)