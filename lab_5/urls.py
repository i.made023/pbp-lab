from django.urls import path, include
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add_note/', add_note, name='add_note'),
    path('note-list/', note_list, name='note_list')
]