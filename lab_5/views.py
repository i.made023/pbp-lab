from django.shortcuts import render
from lab_2.models import Note
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab5_index.html', response)

@login_required(login_url = '/admin/login/')
def add_note(request):
    print(request.POST)
    maps = {}

    to = ""
    fr = ""
    title = ""
    message = ""

    data = dict(request.POST)
    if len(data) > 1:
      to = data['inpTo'][0]
      fr = data['inpFor'][0]
      title = data['inpTitle'][0]
      message = data['inpMessage'][0]
      form = Note.objects.create(To = to, From = fr, Title = title, Message=message)

    if (to!= "" and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-5/')
    else :
        form = ""
        maps['form'] = form
        return render(request, 'lab5_form.html', maps)

@login_required(login_url = '/admin/login/')
def note_list(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab5_note_list.html', response)