import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(20),
          color: Theme.of(context).primaryColor,
          child: Center(
            child: Column(children: <Widget>[
              Container(
                width: 130,
                height: 130,
                margin: EdgeInsets.only(top: 30, bottom: 15),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(
                        'https://media-exp1.licdn.com/dms/image/C5603AQFAvFJJaM19QA/profile-displayphoto-shrink_200_200/0/1630725576571?e=1641427200&v=beta&t=sx3DZWC-l1FEfHIxyTJDotUGUa1YJkvsNOeqMdUbeek'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Text(
                'I Made Indra Mahaarta',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'official.indramahaarta@gmail.com',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ]),
          ),
        ),
        ListTile(
          leading: Icon(Icons.person),
          title: Text(
            'Profile',
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          onTap: null,
        ),
        ListTile(
          leading: Icon(Icons.settings),
          title: Text(
            'Setting',
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.logout),
          title: Text(
            'Logout',
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          onTap: null,
        ),
      ]),
    );
  }
}
