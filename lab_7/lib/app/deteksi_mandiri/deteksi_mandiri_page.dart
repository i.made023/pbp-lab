import 'package:flutter/material.dart';
import 'package:lab_7/app/deteksi_mandiri/create_quiz_page.dart';
import 'package:lab_7/app/deteksi_mandiri/main_drawer/main_drawer_page.dart';

class DeteksiMandiri extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Deteksi Mandiri',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
            )),
        elevation: 5.0,
        centerTitle: true,
      ),
      drawer: MainDrawer(),
      body: _buildContent(context),
      backgroundColor: Colors.grey.shade200,
    );
  }
}

Widget _buildContent(BuildContext context) {
  return Padding(
    padding: EdgeInsets.all(20.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          'Assessment List',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromARGB(223, 8, 16, 58),
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        Divider(
          color: Color.fromARGB(223, 8, 16, 58),
        ),
        SizedBox(
          height: 15.0,
        ),
        TextButton(
          onPressed: () => {
            print("Button Presses!!"),
          },
          child: Text('Covid-19 Test Assessment'),
          style: TextButton.styleFrom(
            textStyle: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        TextButton(
          onPressed: () => {
            print("Button Presses!!"),
          },
          child: Text('Post Test Assessment'),
          style: TextButton.styleFrom(
            textStyle: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        ElevatedButton(
          child: Text('Create Quiz'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateQuiz()),
            );
          },
        ),
      ],
    ),
  );
}
