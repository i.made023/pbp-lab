import 'package:flutter/material.dart';

class CreateQuiz extends StatelessWidget {
  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Quiz Name'),
    );
  }

  Widget _buildType() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Quiz Type'),
    );
  }

  Widget _buildDuration() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Duration'),
    );
  }

  Widget _buildScoreToPass() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Score to Pass'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Deteksi Mandiri',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
            )),
        elevation: 5.0,
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Create Quiz',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.pink,
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              _buildName(),
              _buildType(),
              _buildDuration(),
              _buildScoreToPass(),
              SizedBox(height: 15),
              ElevatedButton(
                  onPressed: () => {print('submit')}, child: Text('submit'))
            ],
          ),
        ),
      ),
      backgroundColor: Colors.grey.shade200,
    );
  }
}
