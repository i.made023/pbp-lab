import 'package:flutter/material.dart';
import 'package:lab_7/app/deteksi_mandiri/deteksi_mandiri_page.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lab_7',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: DeteksiMandiri(),
    );
  }
}