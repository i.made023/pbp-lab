import 'package:flutter/material.dart';
import 'app/sign_in/sign_in_page.dart';
import 'app/deteksi_mandiri/deteksi_mandiri_page.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab_6',
      theme: ThemeData(primarySwatch: Colors.pink),
      home: DeteksiMandiri(),
    );
  }
}
