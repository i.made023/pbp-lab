import 'package:flutter/material.dart';
import 'package:lab_6/app/main_drawer/main_drawer_page.dart';

class DeteksiMandiri extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Deteksi Mandiri',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
            )),
        elevation: 5.0,
        centerTitle: true,
      ),
      drawer: MainDrawer(),
      body: _buildContent(),
      backgroundColor: Colors.grey.shade200,
    );
  }
}

Widget _buildContent() {
  return Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'Covid 19 Test Assessment',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold,
              color: Colors.pink,
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          ElevatedButton(
            onPressed: () {
              print('Button Pressed!');
            },
            child: Text(
              'Start Quiz',
              style: TextStyle(
                fontWeight: FontWeight.normal,
              ),
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.teal),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          ElevatedButton(
              onPressed: () {
                print('Button Pressed!');
              },
              child: Text('Edit Quiz'),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.indigo),
              )),
          SizedBox(
            height: 10.0,
          ),
          ElevatedButton(
              onPressed: () {
                print('Button Pressed!');
              },
              child: Text('Delete Quiz'),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.red),
              )),
        ],
      ));
}
