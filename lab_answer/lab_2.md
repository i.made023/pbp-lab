1. Perbedaan Json dan Xml
  - Json merupakan sebuah format penyimpanan data, sementara itu Xml merupakan sebuah markup languages yang befungsi untuk menyimpan data juga
  - Data dalam json disimpan dalam format map-value, sementara itu di Xml data disimpan dalam format struktur tree/pohon
  - Json tidak dapat melakukan pemrosesan data dan perhitungan apapun, sementara itu Xml mampu melakukan pemrosesan data dan pemformatan dokumen dengan objek
  - Json unggul dalam hal kecepatan penguraian dan transmisi data, sementara itu Xml cenderung lebih lambat dalam hal transmisi pengiriman datanya
  - Struktur mapping pada Json membutanya lebih ringkas dan mudah untuk dibaca serta dipahami. Sementara itu stuktur tag pada Xml cenderung terlihat lebih kompleks. Ini sebenarnya agar subjektive penilaiannya, tetapi saya pribadi merasa kalau xml memang lebih ribet dan kompleks.
  - Json mendukung pengaksesan  data secara langsung dengan array, sementara itu Xml tidak mendukung hal tersebut secara langsung, melainkan harus menggunakan/menambahkan tag untuk setiap item
  - Json hanya mendukung sebagian tipe data primitive dan objek. Bahkan untuk tipe data objek hanya bisa berisikan tipe data primitif saja. Sementara itu xml bisa mendukung struktur data yang lebih kompleks
  - Json mendukung UTF serta ASCII, sementara itu Xml mendukung UTF-8 dan UTF-16

 2. Perbedaan HTML dengan XML
  - Html merupakan singkatan dari hypertext Markup Languages yang berfungsi untuk menampilkan data. Sementara itu XML merupakan markup languages yang berfungsi untuk transfer dan menampung data
  - Tag pada stuktur html sifatnya telah ditentukan dan diformat secara khusus. Namun, kita bisa menggunakan tag apa saja si xml (Bisa di kustomisasi).
  - Html cenderung strict terhadap tag penutupnya. Sementara Xml tidak
  - Html tidak memberikan dukungan namespace, sedangkan Xml memberikan dukungan namespace
  - html bersifat case insensitive, sementara xml besifat case sensitice