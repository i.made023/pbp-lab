from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required


@login_required(login_url = '/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = '/admin/login/')
def add_friends(request):
    c = {}

    form = FriendForm(request.POST or None, request.FILES or None)

    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3/')
    else :
        form = FriendForm
        c['form'] = form
        return render(request, 'lab3_forms.html', c)