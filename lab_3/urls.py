from django.urls import path, include
from .views import index,add_friends

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_friends, name= "add_friends")
]